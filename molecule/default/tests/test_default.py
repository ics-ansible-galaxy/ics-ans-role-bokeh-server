import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_bokeh_server_index(host):
    cmd = host.command("curl -L http://localhost")
    # Title depends if there is one or several app
    assert (
        "<title>Bokeh Application</title>" in cmd.stdout
        or "<title>Running Bokeh Applications</title>" in cmd.stdout
    )
