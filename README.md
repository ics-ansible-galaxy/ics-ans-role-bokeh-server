# ics-ans-role-bokeh-server

Ansible role to install a Bokeh Server.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
bokeh_server_image: registry.esss.lu.se/ics-docker/bokeh-server
bokeh_server_tag: latest
bokeh_server_apps_repo_path: /opt/bokeh-server
bokeh_server_apps_repo_url: https://gitlab.esss.lu.se/ics-infrastructure/bokeh-apps.git
bokeh_server_apps_repo_version: master
bokeh_server_apps_dir: "{{ bokeh_server_apps_repo_path }}/apps"
bokeh_server_epics_ca_addr_list: ""
bokeh_server_hostname: localhost
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-bokeh-server
```

## License

BSD 2-clause
